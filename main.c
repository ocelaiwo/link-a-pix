#include <stdio.h>
#include <stdlib.h>

typedef struct cell cell;
struct cell {
    int x;
    int y;
    cell *next;
};

typedef struct {
    cell *begin;
} way;


int n, m;
short field[100][100];
short checked[100][100];
way ways[10000000];
int correct_ways[100];
int cor_ways_counter = 0;
int ways_counter = 1;


int check_intersect(way *pth, int x, int y) {
    cell *chk = pth->begin;
    while (chk != NULL) {
        if (chk->x == x && chk->y == y) return 1;
        chk = chk->next;
    }
    return 0;
}

void copy(way* from, way* to) {
    to->begin = NULL;
    if(from->begin != NULL) {
        to->begin = malloc(sizeof(*to->begin));
        to->begin->x = from->begin->x;
        to->begin->y = from->begin->y;

        cell* current = to->begin;
        cell * current_val = from->begin->next;
        while (current_val != NULL) {
            current->next = malloc(sizeof(*current->next));
            current->next->x = current_val->x;
            current->next->y = current_val->y;

            current = current->next;
            current_val = current_val->next;
        }
        current->next = NULL;
    }
}

void delete_way(way *w) {
    if (w->begin == NULL) return;
    cell *ptr = w->begin;
    cell *ptr_next = ptr->next;
    while (ptr_next != NULL) {
        free(ptr);
        ptr = ptr_next;
        ptr_next = ptr->next;
    }
    free(ptr);
    w->begin = NULL;
}


void nullify() {
    cor_ways_counter = 0;
    ways_counter = 1;
    for (int i = 0; i < 1000; i++) delete_way(&ways[i]);
}

void dfs(int length, int x, int y, int current_length, int path_n) {
    cell* ptr = ways[path_n].begin;
    cell* ptr_last;
    while(ptr != NULL) {
        ptr_last = ptr;
        ptr = ptr->next;
    }
    if(ways[path_n].begin == NULL) {
        ways[path_n].begin = malloc(sizeof(cell));
        ptr = ways[path_n].begin;
    } else {
        ptr_last->next = malloc(sizeof(cell));
        ptr = ptr_last->next;
    }
    ptr->next = NULL;
    ptr->x = x;
    ptr->y = y;
    if(length < current_length) return;
    if(length == current_length && field[x][y] == length) {
        cor_ways_counter += 1;
        correct_ways[cor_ways_counter-1] = path_n;
        return;
    }
    if((field[x+1][y] == 0 || (field[x+1][y] == length && current_length == length - 1)) && !check_intersect(&ways[path_n],x+1,y) && !checked[x+1][y]) {
        copy(&ways[path_n],&ways[ways_counter]);
        ways_counter++;
//        delete_way(&tmp);
        dfs(length,x+1,y,current_length+1,ways_counter-1);
    }
    if((field[x-1][y] == 0 || (field[x-1][y] == length && current_length == length - 1)) && !check_intersect(&ways[path_n],x-1,y) && !checked[x-1][y]) {
        copy(&ways[path_n],&ways[ways_counter]);
        ways_counter++;
//        delete_way(&tmp);
        dfs(length,x-1,y,current_length+1,ways_counter-1);
    }
    if((field[x][y+1] == 0 || (field[x][y+1] == length && current_length == length - 1)) && !check_intersect(&ways[path_n],x,y+1) && !checked[x][y+1]) {
        copy(&ways[path_n],&ways[ways_counter]);
        ways_counter++;
//        delete_way(&tmp);
        dfs(length,x,y+1,current_length+1,ways_counter-1);
    }
    if((field[x][y-1] == 0 || (field[x][y-1] == length && current_length == length - 1)) && !check_intersect(&ways[path_n],x,y-1) && !checked[x][y-1]) {
        copy(&ways[path_n],&ways[ways_counter]);
        ways_counter++;
//        delete_way(&tmp);
        dfs(length,x,y-1,current_length+1,ways_counter-1);
    }
}

void print_field() {
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if(checked[i][j] < 10) {
                printf("%i  ", checked[i][j]);
            } else {
                printf("%i ", checked[i][j]);
            }
        }
        printf("\n");
    }
    printf("\n");
}

void print_way(way* w) {
    printf("----------------\n");
    cell* ptr = w->begin;
    while(ptr != NULL) {
        printf("%i %i\n",ptr->x,ptr->y);
        ptr = ptr->next;
    }
    printf("----------------\n");
}


void revert_way(way* w) {
    cell *ptr = w->begin;
    while (ptr != NULL) {
        if(ptr != w->begin && ptr->next != NULL) {
            checked[ptr->x][ptr->y] = 0;
        }
        ptr = ptr->next;
    }
}

int check_correct(way* w) {
    cell* ptr = w->begin;
    while(ptr != NULL) {
        if(checked[ptr->x][ptr->y] && ptr != w->begin && ptr->next != NULL) return 1;
        ptr = ptr->next;
    }
    return 0;
}

void apply_way(way* w) {
    cell *ptr = w->begin;
    if(ptr == NULL) return;
    short to_fill = field[ptr->x][ptr->y];
    while(ptr != NULL) {
        checked[ptr->x][ptr->y] = to_fill;
        ptr = ptr->next;
    }
}


int dfs2(int x, int y);

int move_forward(int x, int y) {
    if(y == m) return dfs2(x+1,1);
    else return dfs2(x,y+1);
}


int dfs2(int x, int y) {
    if(field[x][y] == 0 || checked[x][y] != 0) {
        if(x == n && y == m) {
            return 0;
        }
        else return move_forward(x,y);
    }
    nullify();
    dfs(field[x][y],x,y,1,0);
    int local_cor_way = cor_ways_counter;
    int counter = 0;
    while(counter < local_cor_way) {
        nullify();
        dfs(field[x][y],x,y,1,0);
        if(check_correct(&ways[correct_ways[counter]]) == 1) return 1;
        apply_way(&ways[correct_ways[counter]]);
        int success_flag = move_forward(x,y);
        if(success_flag == 0) return 0;
        else revert_way(&ways[correct_ways[counter]]);
        counter++;
    }
    return 1;
}


int main() {
    for (int i = 0; i < 1000; i++) ways[i].begin = NULL;
    scanf("%i%i", &n, &m);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            scanf("%hi", &field[i][j]);
            checked[i][j] = 0;
        }
    }
    for (int i = 0; i <= n + 1; i++) {
        field[i][0] = -1;
        field[i][m + 1] = -1;
    }
    for (int j = 0; j <= m + 1; j++) {
        field[0][j] = -1;
        field[n + 1][j] = -1;
    }
    int a = dfs2(1,1);
    print_field();
    return 0;
}